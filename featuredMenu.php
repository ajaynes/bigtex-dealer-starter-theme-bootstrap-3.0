<div class="col-xs-12 col-sm-9">
	<div class="row">
  	<div class="col-xs-12 col-sm-4">
    	<h3 class="featured text-center"><a href="/inventory-specials/">Specials</a></h3>
    	<div class="boxBT">
        <a href="/inventory-specials/">
        	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/specials.jpg" class="img-responsive" alt="inventory specials" />
        </a>
        <a class="btn btn-primary btn-block bt uppercase" href="/inventory-specials/">View More <span class="fa fa-angle-double-right"></span></a>
        <div class="clearfix"></div>
      </div><!--boxBT-->
    </div><!--col-xs-4-->
    <div class="col-xs-12 col-sm-4">
    	<h3 class="featured text-center"><a href="/cm-truck-beds-inventory/">CM Truck Beds</a></h3>
    	<div class="boxBT">
        <a href="/cm-truck-beds-inventory/">
        	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/ER.png" class="img-responsive" alt="CM Truck Beds" />
        </a>
        <a class="btn btn-primary btn-block bt uppercase" href="/cm-truck-beds-inventory/">View More <span class="fa fa-angle-double-right"></span></a>
        <div class="clearfix"></div>
      </div><!--boxBT-->
    </div><!--col-xs-4-->
    <div class="col-xs-12 col-sm-4">
    	<h3 class="featured text-center"><a href="/videos/">Videos</a></h3>
    	<div class="boxBT">
        <a href="/videos/">
        	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/videos.jpg" class="img-responsive" alt="Videos" />
        </a>
        <a class="btn btn-primary btn-block bt uppercase" href="/videos/">View More <span class="fa fa-angle-double-right"></span></a>
        <div class="clearfix"></div>
      </div><!--boxBT-->
    </div><!--col-xs-4-->
  </div><!--row-->
</div>