<?php
/*
Template Name: Inventory Search
*/
?>
<?php get_header(); ?>
<?php get_template_part( 'slider' ); ?>
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'page' ); ?>
    <?php get_sidebar('search'); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>