<div id="slider" class="carousel slide">
  <div class="carousel-inner">
      <div class="item active">
          <?php if ( of_get_option('slide1') ) { 
                  if (of_get_option('slidelink1')){
                  echo '<a href="'; echo of_get_option('slidelink1');
                    if (of_get_option('linktarget1')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide1'); echo '" alt="'; echo of_get_option('alt1'); echo '" />'; 
                  if (of_get_option('slidelink1')){echo '</a>';}
					}
					else { echo '<img src="'; echo bloginfo('stylesheet_directory'); echo '/images/slides/utility-trailers.jpg" alt="Utility Trailer" />';
              }?>
          <div class="carousel-caption">
              <?php if ( of_get_option('captitle1') ) { ?>
                  <h4><?php echo of_get_option('captitle1'); ?></h4>
              <?php } ?>
              <?php if ( of_get_option('captext1') ) { ?>
                  <p><?php echo of_get_option('captext1'); ?></p>
              <?php } ?>
          </div><!--carousel-caption-->
      </div>
      <!--item-->
      
      <div class="item">
            <?php if ( of_get_option('slide2') ) { 
                  if (of_get_option('slidelink2')){
                  echo '<a href="'; echo of_get_option('slidelink2');
                    if (of_get_option('linktarget2')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide2'); echo '" alt="'; echo of_get_option('alt2'); echo '" />'; 
                  if (of_get_option('slidelink2')){echo '</a>';}
						}
						else { echo '<img src="'; echo bloginfo('stylesheet_directory'); echo '/images/slides/dump-trailer.jpg" alt="Dump Trailer" />';
              }?>
          <div class="carousel-caption">
              <?php if ( of_get_option('captitle2') ) { ?>
                  <h4><?php echo of_get_option('captitle2'); ?></h4>
              <?php } ?>
              <?php if ( of_get_option('captext2') ) { ?>
                  <p><?php echo of_get_option('captext2'); ?></p>
              <?php } ?>
          </div><!--carousel-caption-->
      </div>
      <!--item-->
      
      <div class="item">
          <?php if ( of_get_option('slide3') ) { 
                  if (of_get_option('slidelink3')){
                  echo '<a href="'; echo of_get_option('slidelink3');
                    if (of_get_option('linktarget3')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide3'); echo '" alt="'; echo of_get_option('alt3'); echo '" />'; 
                  if (of_get_option('slidelink3')){echo '</a>';}
					}
					else { echo '<img src="'; echo bloginfo('stylesheet_directory'); echo '/images/slides/gooseneck-trailers.jpg" alt="Gooseneck Trailers" />';
              }?>
          <div class="carousel-caption">
              <?php if ( of_get_option('captitle3') ) { ?>
                  <h4><?php echo of_get_option('captitle3'); ?></h4>
              <?php } ?>
              <?php if ( of_get_option('captext3') ) { ?>
                  <p><?php echo of_get_option('captext3'); ?></p>
              <?php } ?>
          </div><!--carousel-caption-->
      </div>
      <!--item-->
      
      <?php if ( of_get_option('slide4') ) { ?>
          <div class="item">
               <?php if ( of_get_option('slide4') ) { 
                  if (of_get_option('slidelink4')){
                  echo '<a href="'; echo of_get_option('slidelink4');
                    if (of_get_option('linktarget4')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide4'); echo '" alt="'; echo of_get_option('alt4'); echo '" />'; 
                  if (of_get_option('slidelink4')){echo '</a>';}
              }?>
              <div class="carousel-caption">
                  <?php if ( of_get_option('captitle4') ) { ?>
                      <h4><?php echo of_get_option('captitle4'); ?></h4>
                  <?php } ?>
                  <?php if ( of_get_option('captext4') ) { ?>
                      <p><?php echo of_get_option('captext4'); ?></p>
                  <?php } ?>
              </div><!--carousel-caption-->
          </div>
      <?php }?>
      <!--item-->
      
      <?php if ( of_get_option('slide5') ) { ?>
          <div class="item">
               <?php if ( of_get_option('slide5') ) { 
                  if (of_get_option('slidelink5')){
                  echo '<a href="'; echo of_get_option('slidelink5');
                    if (of_get_option('linktarget5')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide5'); echo '" alt="'; echo of_get_option('alt5'); echo '" />'; 
                  if (of_get_option('slidelink5')){echo '</a>';}
              }?>
              <div class="carousel-caption">
                  <?php if ( of_get_option('captitle5') ) { ?>
                      <h4><?php echo of_get_option('captitle5'); ?></h4>
                  <?php } ?>
                  <?php if ( of_get_option('captext5') ) { ?>
                      <p><?php echo of_get_option('captext5'); ?></p>
                  <?php } ?>
              </div><!--carousel-caption-->
          </div>
      <?php }?>
      <!--item-->
      
      <?php if ( of_get_option('slide6') ) { ?>
          <div class="item">
              <?php if ( of_get_option('slide6') ) { 
                  if (of_get_option('slidelink6')){
                  echo '<a href="'; echo of_get_option('slidelink6');
                    if (of_get_option('linktarget6')==1){
                      echo '"target="_blank">';} else{ echo '"target="_self">';} 
                  }
                  echo '<img src="'; echo of_get_option('slide6'); echo '" alt="'; echo of_get_option('alt6'); echo '" />'; 
                  if (of_get_option('slidelink6')){echo '</a>';}
              }?>
              <div class="carousel-caption">
                  <?php if ( of_get_option('captitle6') ) { ?>
                      <h4><?php echo of_get_option('captitle6'); ?></h4>
                  <?php } ?>
                  <?php if ( of_get_option('captext6') ) { ?>
                      <p><?php echo of_get_option('captext6'); ?></p>
                  <?php } ?>
              </div><!--carousel-caption-->
          </div>
      <?php }?>
      <!--item--> 
  </div><!--carousel inner-->   
</div><!--slider-->