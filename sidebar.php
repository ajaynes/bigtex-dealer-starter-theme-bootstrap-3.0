<div class="col-xs-12 col-sm-4 col-sm-pull-8">
    <aside>
        <?php if ( is_active_sidebar( 'main' ) ) : ?>
            <?php dynamic_sidebar( 'main' ); ?>
        <?php endif; ?>
    </aside>
</div>