jQuery(document).ready(function(){
	jQuery('#slider', '#trailer').carousel();
	jQuery("footer li.facebook a, footer li.google a, footer li.twitter a, footer li.youtube a").each(function () {
    jQuery(this).html(jQuery(this).text(''));
	});
	jQuery('footer .google a').addClass('fa fa-google-plus-square fa-3x');
	jQuery('footer .facebook a').addClass('fa fa-facebook-square fa-3x');
	jQuery('footer .twitter a').addClass('fa fa-twitter-square fa-3x');
	jQuery('footer .youtube a').addClass('fa fa-youtube-square fa-3x');
	jQuery('.carousel-caption:not(:has(h4))').hide();
	//remove the ability to use "p align="
	jQuery('*').each(function(){
		jQuery(this).removeAttr('align')
	});
	// Show or hide the sticky footer button
	jQuery(window).scroll(function(){
		if (jQuery(this).scrollTop() > 200){
			jQuery('.go-top').fadeIn(200);
		} else{
			jQuery('.go-top').fadeOut(200);
		}
	});
	// Animate the scroll to top
	jQuery('.go-top').click(function(event){
		event.preventDefault();
		jQuery('html, body').animate({scrollTop: 0}, 300);
	})
	jQuery('.ts_search h1').replaceWith(function(){
    return jQuery('<span class="title" />').append(jQuery(this).contents());
	});
	jQuery('.ts_search ul li').wrap('<label></label>');
	jQuery('.ts_search ul label').wrap('<div class="checkbox"></div>');
	jQuery('.ts_search select').unwrap();
	jQuery('.ts_search select, .ts_search input[type="text"]').addClass('form-control');
	jQuery('.ts_search button').addClass('btn btn-lg btn-primary btn-block');
	jQuery('.gfield_contains_required.hidden_label .ginput_container label').append('<span class="gfield_required">*</span>');
	jQuery('.gform_button').addClass('btn btn-lg btn-block btn-primary');
});