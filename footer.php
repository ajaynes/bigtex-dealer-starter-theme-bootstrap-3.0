</div>
<!--container-->
<footer>
  <div class="container">
    <?php if ( of_get_option('footer_uploader') ) { ?>
    <div class="row">
      <div class="col-xs-12"> <img src="<?php echo of_get_option('footer_uploader'); ?>" class="img-responsive aligncenter" alt="Trailer Brands We Proudly Carry" /> </div>
      <!--col-xs-12 center--> 
    </div>
    <!--row-->
    <?php } ?>
    <div class="row">
      <div class="col-xs-12 col-sm-8">
        <p class="disclaimer">&copy;<?php echo date('Y'); ?> <a href="http://www.bigtextrailers.com/" title="Big Tex Trailers Marketing" rel="nofollow" target="_blank">Big Tex Trailers Marketing</a>. All Rights Reserved. All text, images, logos, content, design, and coding of this Web site is protected by all applicable copyright and trademark laws.</p>
        <p><a href="/privacy-policy/" title="View our privacy policy">Privacy Policy</a></p>
      </div>
      <!--col-sm-8-->
      <div class="col-xs-12 col-sm-4 text-center">
        <?php wp_nav_menu(array(
						'theme_location' => 'social',
						'container' => 'nav',
						'menu_class' => 'list-inline list-unstyled',
						'fallback_cb' => false
					));
				?>
        <p class="bigTexPower"><a href="http://www.bigtextrailers.com/" title="Big Tex Trailers Marketing" rel="nofollow" target="_blank">Powered by BigTex Trailers - Marketing</a></p>
      </div>
      <!--col-xs-4 center--> 
    </div>
    <!--row--> 
  </div>
  <!--container--> 
</footer>
<?php wp_footer(); ?>
</body></html>