<?php
/*
Template Name: Custom Page
*/
?>
<?php get_header(); ?>
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', 'custom' ); ?>
    <?php get_sidebar(); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>