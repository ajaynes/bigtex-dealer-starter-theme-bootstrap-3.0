<div class="col-xs-12">
	<div class="row">
  	<?php if ( is_active_sidebar( 'featured1' ) ) : ?>
    	<?php dynamic_sidebar( 'featured1' ); ?>
    <?php endif; ?>
    
    <?php if ( is_active_sidebar( 'featured2' ) ) : ?>
    	<?php dynamic_sidebar( 'featured2' ); ?>
    <?php endif; ?>
    
    <?php if ( is_active_sidebar( 'featured3' ) ) : ?>
    	<?php dynamic_sidebar( 'featured3' ); ?>
    <?php endif; ?>
  </div>
</div><!--col-xs-12-->