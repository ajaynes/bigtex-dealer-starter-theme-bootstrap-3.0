<nav class="navbar navbar-default bt" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bt-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>
    <?php wp_nav_menu( array(
        'menu'      		=> 'main',
				'theme_location' => 'main',
        'depth'     		=> 2,
        'container'  		=> 'div',
        'container_class'   => 'collapse navbar-collapse',
        'container_id'      => 'bt-collapse',
        'menu_class'        => 'nav navbar-nav bt',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker())
      );
    ?>
</nav>