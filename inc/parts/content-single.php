<div class="col-xs-12 col-sm-8 col-sm-push-4">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    	<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
        <p>Posted on <?php the_time('F jS, Y'); ?> by <?php the_author(); ?></p>
        <section class="well">
        	<?php the_content('Read more on "'.the_title('', '', false).'" &raquo;'); ?>
        </section>
        <?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
		<?php the_tags( '<p>Tags: ', ', ', '</p>'); ?>
        <p>This entry was posted on <?php the_time('l, F jS, Y'); ?> at <?php the_time(); ?> and is filed under <?php the_category(', ') ?>.</p>
        <nav>
			<?php previous_post_link(); ?> &bull; <?php next_post_link(); ?>
		</nav>
    <?php endwhile; else: ?>
    	<p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>
</div>