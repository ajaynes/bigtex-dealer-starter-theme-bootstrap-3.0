<div class="col-xs-12 col-sm-8 col-sm-push-4">
    <section class="post">
    <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
            <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
            <p>Posted on <?php the_time('F jS, Y'); ?> by <?php the_author(); ?></p>
        <section>
            <?php the_content('Read more on "'.the_title('', '', false).'" &raquo;'); ?>
        </section>
            <p><?php the_tags('Tags: ', ', ', '<br>'); ?> Posted in <?php the_category(', '); ?> &bull; <?php edit_post_link('Edit', '', ' &bull; '); ?> <?php comments_popup_link('Respond to this post &raquo;', '1 Response &raquo;', '% Responses &raquo;'); ?></p>
    </section>

    <?php endwhile; ?>

    <nav>
        <?php posts_nav_link('&nbsp;&bull;&nbsp;'); ?>
    </nav>

    <?php else : ?>

    <section class="post">
        <h2>Not Found</h2>
        <p>Sorry, but the requested resource was not found on this site.</p>
    </section>

    <?php endif; ?>
</div>