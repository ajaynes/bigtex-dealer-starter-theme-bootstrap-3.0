<div class="col-xs-12 col-sm-8 col-sm-push-4">
    <article>
        <h1>Oh No! Page not found :(</h1>
        <p>Please return to the <a href="<?php bloginfo('url'); ?>/">homepage</a>.</p>
        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/404.jpg" alt="404" class="img-responsive" />
    </article>
</div><!--span8-->