<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); ?>
<div class="content">
  <div class="row">
  	<div class="col-xs-12 col-sm-9">
    	<?php get_template_part( 'slider' ); ?>
    </div>
    <?php get_template_part( 'buttons' ); ?>
  </div><!--row-->
  <div class="row">
  	<?php get_template_part( 'featuredMenu' ); ?>
    <?php get_sidebar('home'); ?>
  </div><!--row-->
  <div class="row">
  	<?php get_template_part( '/inc/parts/content', 'homepageText' ); ?>
  </div><!--row-->
  <div class="row">
  	<?php get_template_part( 'trailerSlider' ); ?>
  </div><!--row-->
  <div class="row">
    <?php get_template_part( '/inc/parts/content', 'home' ); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>