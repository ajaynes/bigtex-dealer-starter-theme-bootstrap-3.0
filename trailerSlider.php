<div class="col-xs-12">
  <div class="trailerSlide">
    <h3>Trailer Styles</h3>
    <div class="carousel slide" id="trailer">
      <div class="row">
        <div class="carousel-inner">
          <div class="item active">
            <div class="col-xs-12 col-sm-4">
              <div class="thumbnail bt">
                <a href="/automotorcycle-haulers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/autohauler.png" class="img-responsive" alt="Auto Hauler" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/automotorcycle-haulers/">Auto Haulers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-4-->
            <div class="col-xs-12 col-sm-4">
              <div class="thumbnail bt">
                <a href="/cmtruck-beds/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/cmtruckbeds.png" class="img-responsive" alt="CM Truck Beds" />
                </a>
              </div><!--thumbnail bt-->
            	<div class="caption">
                <a href="/cmtruck-beds/">CM Truck Beds</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-4-->
            <div class="col-xs-12 col-sm-4">
              <div class="thumbnail bt">
                <a href="/gooseneck-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/gooseneck.png" class="img-responsive" alt="Gooseneck Trailer" />
                </a>
              </div><!--thumbnail bt-->
             	<div class="caption">
                <a href="/gooseneck-trailers/">Gooseneck Trailers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-4-->
          </div><!--item-->
          <div class="item">
            <div class="col-xs-12 col-sm-4">
              <div class="thumbnail bt">
                <a href="/cm-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/cmtrailers.png" class="img-responsive" alt="CM Trailers" />
                </a>  
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/cm-trailers/">CM Trailers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-4-->
            <div class="col-xs-12 col-sm-4">
              <div class="thumbnail bt">
                <a href="/single-axle-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/singleaxle.png" class="img-responsive" alt="Enclosed Trailer" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/single-axle-trailers/">Single Axle Trailers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-4-->
            <div class="col-xs-12 col-sm-4">
              <div class="thumbnail bt">
                <a href="/landscape-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/landscape.png" class="img-responsive" alt="Landscape Trailer" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/landscape-trailers/">Landscape Trailers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-4-->
          </div><!--item2-->
          <div class="item">
            <div class="col-xs-12 col-sm-4">
              <div class="thumbnail bt">
                <a href="/dump-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/dump.png" class="img-responsive" alt="Dump Trailer" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/dump-trailers/">Dump Trailers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-4-->
            <div class="col-xs-12 col-sm-4">
              <div class="thumbnail bt">
                <a href="/tandem-axle-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/tandemaxle.png" class="img-responsive" alt="Tandem Axle Trailer" />
                </a>
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/tandem-axle-trailers/">Tandem Axle Trailers</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-4-->
            <div class="col-xs-12 col-sm-4">
              <div class="thumbnail bt">
                <a href="/heavy-equipment-trailers/">
                  <img src="<?php bloginfo('stylesheet_directory'); ?>/images/heavyequipment.png" class="img-responsive" alt="Heavy Equipment" />
                </a>  
              </div><!--thumbnail bt-->
              <div class="caption">
                <a href="/heavy-equipment-trailers/">Heavy Equipment</a>
              </div><!--caption-->
            </div><!--col-xs-12 col-sm-4-->
          </div><!--item-->
        </div><!--carousel-inner-->
      </div><!--row-->
      <a class="left carousel-control hidden-xs bt" href="#trailer" data-slide="prev">
    		<span class="fa-stack left">
        	<span class="fa fa-square fa-stack-2x"></span>
        	<span class="fa fa-chevron-left fa-stack-1x fa-inverse"></span>
        </span>
  		</a>
  		<a class="right carousel-control hidden-xs bt" href="#trailer" data-slide="next">
      	<span class="fa-stack right">
        	<span class="fa fa-square fa-stack-2x"></span>
    			<span class="fa fa-chevron-right fa-stack-1x fa-inverse"></span>
        </span>
  		</a>
    </div><!--carousel slide-->
  </div><!--trailerSlide-->
</div><!--col-xs-12-->