<?php get_header(); ?>
<?php get_template_part( 'slider' ); ?>
<div class="content">
	<div class="row">
    <?php get_template_part( '/inc/parts/content', '404' ); ?>
    <?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>