<!DOCTYPE html>
<html <?php language_attributes(); ?>>
		<!--[if lte IE 8 ]> <html <?php language_attributes(); ?> class="ie8"> <![endif]--><head>
		<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
		<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><!--add comment before this meta tag if this site is to be fixed-->
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<div class="container">
    <a href="#" class="go-top hidden-xs hidden-sm"><span class="fa fa-arrow-circle-o-up fa-2x fa-fw"></span></a>
		<header>
    	<div class="row">
        <h2 class="col-xs-12 col-sm-6 col-md-4">
          <a href="<?php bloginfo('url'); ?>/">
            <!--gets image for logo from theme options-->
            <?php if ( of_get_option('header_uploader') ) { ?>
              <img src="<?php echo of_get_option('header_uploader'); ?>" class="img-responsive aligncenter" alt="<?php bloginfo('name'); ?>" />
            <?php } ?>
          </a>
        </h2>
        <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-4">
          <?php if ( of_get_option('phone1') ) { ?>
            <div class="phoneNumber">
              <?php echo of_get_option('phone1'); ?>
            </div><!--phoneNumber-->
          <?php } ?>
          <div class="addy">
            <?php if ( of_get_option('address1') ) { ?>
              <p><?php echo of_get_option('address1'); ?></p>
            <?php } ?>
            <?php if ( of_get_option('address2') ) { ?>
              <p><?php echo of_get_option('address2'); ?></p>
            <?php } ?>
          </div><!--addy-->
        </div><!--col-xs-12 col-sm-6 col-md-4 col-md-offset-4-->
      </div><!--row-->
		</header>
  	<?php get_template_part( '/inc/parts/menu', 'nav' ); ?>