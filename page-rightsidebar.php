<?php
/*
Template Name: Sidebar Right
*/
?>
<?php get_header(); ?>
<?php get_template_part( 'slider' ); ?>
<div class="content">
	<div class="row">
		<?php get_template_part( '/inc/parts/content', 'pageright' ); ?>
    <?php get_sidebar('right'); ?>
  </div><!--row-->
</div><!--content-->
<?php get_footer(); ?>